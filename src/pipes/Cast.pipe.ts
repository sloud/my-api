import { BadRequestException, PipeTransform } from '@nestjs/common'
import { z } from 'zod'

export class Cast<A extends z.ZodTypeAny>
  implements PipeTransform<any, z.infer<A>>
{
  constructor(private readonly schema: A) {}

  static as<A extends z.ZodTypeAny>(schema: A): Cast<A> {
    return new Cast(schema)
  }

  transform(value: unknown): z.infer<A> {
    const result = this.schema.safeParse(value)

    if (result.success === false) {
      throw new BadRequestException(result.error.issues)
    }

    return result.data
  }
}
