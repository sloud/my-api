import { z } from 'zod'

const MyDtoBase = z.object({
  name: z.string(),
  pies: z.array(z.string()),
})

export const MyDtoV0 = MyDtoBase.extend({
  version: z.undefined(),
})

export type MyDtoV0 = z.infer<typeof MyDtoV0>

export const MyDtoV1 = MyDtoBase.extend({
  version: z.literal('v1'),
  sides: z.string().array(),
  nested: z
    .object({
      value: z.string().array(),
    })
    .optional(),
})

export type MyDtoV1 = z.infer<typeof MyDtoV1>

export const MyDtoHistorical = z.discriminatedUnion('version', [
  MyDtoV0,
  MyDtoV1,
])

export type MyDtoHistorical = z.infer<typeof MyDtoHistorical>
