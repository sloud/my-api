import {
  Body,
  Controller,
  Get,
  Logger,
  Param,
  Post,
  NotFoundException,
} from '@nestjs/common'
import { AppService } from './app.service'
import { Cast } from './pipes/Cast.pipe'
import { MyDtoHistorical, MyDtoV0, MyDtoV1 } from './app.dtos'

@Controller()
export class AppController {
  private readonly logger = new Logger(AppController.name)

  constructor(private readonly appService: AppService) {}

  @Get()
  allMenus() {
    return this.appService.getAll()
  }

  @Get(':id')
  getMenu(@Param('id') id: string): MyDtoHistorical {
    const menu = this.appService.getMenu(id)

    if (menu === undefined) {
      throw new NotFoundException()
    }

    return menu
  }

  @Post(':id')
  setMenuAny(
    @Body(Cast.as(MyDtoHistorical)) myDto: MyDtoHistorical,
    @Param('id') id: string,
  ) {
    if (myDto.version === undefined) {
      this.logger.log('v0 dto created')
    } else if (myDto.version === 'v1') {
      this.logger.log('v1 dto created')
    }
    this.appService.setMenu(id, myDto)
  }

  @Post('v0/:id')
  setMenu(@Body(Cast.as(MyDtoV0)) myDto: MyDtoV0, @Param('id') id: string) {
    this.appService.setMenu(id, myDto)
  }

  @Post('v1/:id')
  setMenuV1(@Body(Cast.as(MyDtoV1)) myDto: MyDtoV1, @Param('id') id: string) {
    this.appService.setMenu(id, myDto)
  }
}
