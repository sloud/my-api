import { Injectable } from '@nestjs/common'
import { MyDtoHistorical } from './app.dtos'

@Injectable()
export class AppService {
  private readonly menus: Record<string, MyDtoHistorical> = {}

  getHello(): string {
    return 'Hello World!'
  }

  getMenu(id: string): MyDtoHistorical {
    return this.menus[id]
  }

  setMenu(id: string, dto: MyDtoHistorical) {
    this.menus[id] = dto
  }

  getAll() {
    return this.menus
  }
}
